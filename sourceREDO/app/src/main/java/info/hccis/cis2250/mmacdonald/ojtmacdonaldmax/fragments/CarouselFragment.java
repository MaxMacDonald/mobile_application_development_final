package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.R;

/**
 * Author: Max MacDonald
 * Since: 1/25/2019
 * This fragment contains the logic for running a carousel widget.
 */
public class CarouselFragment extends Fragment
{

    private int[] carouselImages = new int[]
            {
                    R.drawable.carousel1,R.drawable.carousel2,R.drawable.carousel3,R.drawable.carousel4,
                    R.drawable.carousel5
            };

    private String[] carouselImageTitles = new String[]
            {
                    "Thumbs Up!","Proofreading...","Good Meeting.","Networking.","Stocks Are Up!"
            };

    public CarouselFragment() { }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_carousel, container, false);
        final CarouselView carousel = view.findViewById(R.id.carousel);
        carousel.setPageCount(carouselImages.length);

        carousel.setImageListener(new ImageListener()
        {
            @Override
            public void setImageForPosition(int position, ImageView imageView)
            {
                imageView.setImageResource(carouselImages[position]);
            }
        });

        carousel.setImageClickListener(new ImageClickListener()
        {
            @Override
            public void onClick(int position)
            {
                Toast.makeText(CarouselFragment.this.getContext(), carouselImageTitles[position], Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}