package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments.BusinessListFragment;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments.CarouselFragment;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.util.Utility;

/**
 * Author: Max MacDonald
 * Since: 1/18/2019
 * This class contains the main logic for the ojt application.
 */


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{

    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //send the user to the carousel fragment as a welcome page.
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.main_fragment,new CarouselFragment()).commit();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
            {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            //Send user to the about page.
            case R.id.action_about:
                AboutActivity newAbout = new AboutActivity();
                Intent aboutIntent = new Intent(getApplicationContext(), newAbout.getClass());
                this.startActivity(aboutIntent);
                return true;
            //Send user to the help page.
            case R.id.action_help:
                Intent helpIntent = new Intent(getApplicationContext(), HelpActivity.class);
                this.startActivity(helpIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //replace current fragment with the business list fragment.
        if (id == R.id.nav_showBusinesses)
        {
            //Log.d("MAXMAC","Business button pressed.");
            MainActivity.fragmentManager.beginTransaction().replace(R.id.main_fragment,
                    new BusinessListFragment()).addToBackStack(null).commit();
        }
        if (id == R.id.nav_share)
        {
            /**
             * This code was taken and refactored from Darcy Watts' social media integration portion
             * of the CIS2250 research project.
             */
            Log.d("MAXMAC","Share button pressed.");
            String message = "About to go on OJT for holland college!";
            // Create intent using ACTION_VIEW and a normal Twitter url:
            String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                    Utility.urlEncode(message),
                    Utility.urlEncode(""));
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));
            //If twitter app is installed, send user to tweet there.
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches)
            {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter"))
                {
                    intent.setPackage(info.activityInfo.packageName);
                }
            }
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}