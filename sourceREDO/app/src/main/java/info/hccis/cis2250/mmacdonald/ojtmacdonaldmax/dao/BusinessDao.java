package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Author: Max MacDonald
 * Since: 1/25/2019
 * This interface contains the database methods used by the room database.
 */

@Dao
public interface BusinessDao
{
    @Insert
    public void addBusiness(Business business);

    @Query("select * from businesses")
    public List<Business> getBusinesses();

    @Delete
    public void deleteBusiness(Business business);

    @Update
    public void updateBusiness(Business business);

    @Query("delete from businesses")
    public void deleteAllBusinesses();
}