package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.R;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao.Business;

/**
 * Author: Max MacDonald
 * Since: 2/10/2019
 * This fragment contains the logic for creating a list of information about a business object.
 */

public class BusinessDetailsFragment extends Fragment
{
    private static String ARG_PARAM1 = "Business";
    private String mParam1;

    private EditText businessName, businessWebsite, businessPhone, businessPositions;
    private Button addContact;

    public BusinessDetailsFragment() { }

    public static BusinessDetailsFragment newInstance(Business business)
    {
        BusinessDetailsFragment fragment = new BusinessDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, business);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        final Bundle tempArgs = getArguments();
        Business business = (Business) tempArgs.getSerializable(ARG_PARAM1);
        Log.d("MAXMAC","Business details: "+business.toString());
        //reference the text fields in the layout.
        businessName = getView().findViewById(R.id.text_business_name);
        businessWebsite = getView().findViewById(R.id.text_business_website);
        businessPhone = getView().findViewById(R.id.text_business_phoneNum);
        businessPositions = getView().findViewById(R.id.text_business_positions);
        addContact = getView().findViewById(R.id.button_add_contact);

        businessName.setText(business.getName());

        //prepare website link.
        businessWebsite.setText(Html.fromHtml(business.getWebsite()));
        businessWebsite.setMovementMethod(LinkMovementMethod.getInstance());

        //prepare phone number link.
        businessPhone.setText(business.getPhoneNumber());
        Linkify.addLinks(businessPhone, Linkify.ALL);

        String positionNum = business.getNumberOfPositions()+"";
        businessPositions.setText(positionNum);

        businessName.setTextColor(Color.BLACK);
        businessWebsite.setTextColor(Color.BLACK);
        businessPhone.setTextColor(Color.BLACK);
        businessPositions.setTextColor(Color.BLACK);


        //if the add contact button is clicked, send the user to a pre-filled contact add page.
        addContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*
                 * Code taken and refactored from: https://developer.android.com/training/contacts-provider/modify-data
                 */
                // Creates a new Intent to insert a contact.
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                //adds values from the edit text fields into the new contact add page.
                //concatenated an empty string to the end of businessName.getText() so it inserts properly.
                intent.putExtra(ContactsContract.Intents.Insert.NAME, businessName.getText()+"")
                        .putExtra(ContactsContract.Intents.Insert.PHONE, businessPhone.getText());
                //Log.d("MAXMAC",businessName.getText()+"");
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_business_details, container, false);
    }
}