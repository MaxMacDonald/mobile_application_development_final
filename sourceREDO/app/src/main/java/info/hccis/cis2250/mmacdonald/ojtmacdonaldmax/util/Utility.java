package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Author: Max MacDonald
 * Since: 2/6/2019
 * This class contains various functions associated with utility in the ojt project.
 */


public class Utility
{
    public static void showDialog(final Activity activity, String title, CharSequence message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });
//        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(activity.getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
//            }
//        });
        builder.show();
    }

    //Encodes the URL
    public static String urlEncode(String s)
    {
        try
        {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            Log.e("MAXMAC", "UTF-8 not supported.", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

}