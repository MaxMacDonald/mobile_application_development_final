package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Author: Max MacDonald
 * Since: 2/6/2019
 * This class contains the code for running a new about activity.
 */

public class AboutActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}