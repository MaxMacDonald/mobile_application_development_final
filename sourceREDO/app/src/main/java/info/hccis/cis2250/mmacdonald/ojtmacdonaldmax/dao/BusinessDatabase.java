package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Author: Max MacDonald
 * Since: 1/24/2019
 * This abstract class declares the room database itself.
 */

@Database(entities = {Business.class},version = 1)
public abstract class BusinessDatabase extends RoomDatabase
{
    public abstract BusinessDao BusinessDao();
}