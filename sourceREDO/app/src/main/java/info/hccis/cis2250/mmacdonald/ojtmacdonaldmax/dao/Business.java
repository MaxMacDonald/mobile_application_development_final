package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

/**
 * Author: Max MacDonald
 * Since: 1/24/2019
 * This class contains attributes and methods associated with a business object.
 */

@Entity (tableName = "businesses")
public class Business implements Serializable {
    @PrimaryKey
    private int id;

    private String username;

    private String name;

    private String phoneNumber;

    private String website;

    private int numberOfPositions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getNumberOfPositions() {
        return numberOfPositions;
    }

    public void setNumberOfPositions(int numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    @Override
    public String toString()
    {
        return "Business{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", website='" + website + '\'' +
                ", numberOfPositions=" + numberOfPositions +
                '}';
    }
}