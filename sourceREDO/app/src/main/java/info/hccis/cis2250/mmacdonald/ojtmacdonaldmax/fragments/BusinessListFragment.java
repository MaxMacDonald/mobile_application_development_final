package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments;

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.R;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.adapter.BusinessAdapter;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao.Business;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao.BusinessDatabase;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.util.Utility;

import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Author: Max MacDonald
 * Since: 2/8/2019
 * This fragment contains the logic for creating a list of business objects in a recyclerview.
 */


public class BusinessListFragment extends Fragment
{
    private RecyclerView recyclerView;
    private BusinessAdapter businessAdapter;
    private List<Business> businessList = new ArrayList<Business>();
    private static BusinessDatabase businessDatabase;


    public BusinessListFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_business_list, container, false);
        return view;
    }

    public void onStart()
    {
        super.onStart();
        final Bundle tempArgs = getArguments();
        recyclerView = getView().findViewById(R.id.recyclerViewBusinesses);
        businessAdapter = new BusinessAdapter(getActivity(), businessList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(businessAdapter);
        //make sure the database is created...
        businessDatabase = Room.databaseBuilder(getContext(),BusinessDatabase.class, "businesses").allowMainThreadQueries().build();
        new HttpRequestTask().execute();
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, Business[]>
    {
        @Override
        protected Business[] doInBackground(Void... params)
        {
            try
            {
                final String url = getString(R.string.business_web_service_url);
                Log.d("MAXMAC", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
                converter.setSupportedMediaTypes(Arrays.asList(new MediaType[]{MediaType.ALL}));
                messageConverters.add(converter);
                restTemplate.setMessageConverters(messageConverters);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Business[] businesses = null;
                try
                {
                    businesses = restTemplate.getForObject(url, Business[].class);
                    //Log.d("MAXMAC","First business object: "+businesses[0]);
                }
                catch (Exception e)
                {
                    Log.e("MAXMAC", "error getting businesses: "+e.getMessage(), e);
                }
                return businesses;
            }
            catch (Exception e)
            {
                Log.e("MAXMAC", "Exception caught: "+e.getMessage(), e);
            }
            return null;
        }

        protected void onPostExecute(Business[] businesses)
        {
            //****************************************************************************
            // Refresh the database
            //****************************************************************************
            if(businesses!= null && businesses.length>0)
            {
                businessList.clear();
                businessList.addAll(Arrays.asList(businesses));
                Log.d("MAXMAC", "database size="+BusinessListFragment.businessDatabase.BusinessDao().getBusinesses().size());
                ArrayList<Business> theBusinesses = new ArrayList<Business>(BusinessListFragment.businessDatabase.BusinessDao().getBusinesses());
                //delete
                BusinessListFragment.businessDatabase.BusinessDao().deleteAllBusinesses();
                //insert all
                for(Business business: businesses)
                {
                    BusinessListFragment.businessDatabase.BusinessDao().addBusiness(business);
                }
                //added snackbar to show when database is updated.
                /*
                * This snackbar will crash the program if the user quickly presses the back button twice from
                * the details screen.
                * Can try handle the error if there is time.
                */
                Snackbar snackbar = Snackbar.make(getView(), "Businesses updated.", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
            //could not connect so get from the room database.
            else
                {
                Log.d("MAXMAC","loading from room database.");

                ArrayList<Business> theBusinesses = new ArrayList<Business>(BusinessListFragment.businessDatabase.BusinessDao().getBusinesses());
                businessList.clear();
                businessList.addAll(theBusinesses);
            }
            //this line ensures that the page is refreshed every time.
            businessAdapter.notifyDataSetChanged();
        }
    }
}