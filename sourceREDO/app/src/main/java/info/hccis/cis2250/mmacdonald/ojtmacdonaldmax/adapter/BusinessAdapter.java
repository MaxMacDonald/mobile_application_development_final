package info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.R;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.dao.Business;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.fragments.BusinessDetailsFragment;
import info.hccis.cis2250.mmacdonald.ojtmacdonaldmax.util.Utility;

/**
 * Author: Max MacDonald
 * Since: 2/6/2019
 * This class converts a regular java arraylist into a listable set of objects on android.
 *
 */


public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.MyViewHolder>
{
    private List<Business> businessList;
    private Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.business_row, parent, false);
        Log.d("MAXMAC", "Inside onCreateViewHolder method - business_row inflated.");
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        final Business business = businessList.get(position);
        holder.businessName.setText(business.getName());
        holder.businessWebsite.setText(business.getWebsite());
        holder.businessPhoneNum.setText(business.getPhoneNumber());
    }

    public BusinessAdapter(Context context, List<Business> businesses)
    {
        Log.d("MAXMAC","Instantiating adapter.");
        this.context = context;
        this.businessList = businesses;
    }

    @Override
    public int getItemCount()
    {
        return businessList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView businessName, businessWebsite, businessPhoneNum;

        public MyViewHolder(View view)
        {
            super(view);
            Log.d("MAXMAC","Entered View Holder.");
            businessName = view.findViewById(R.id.textViewBusinessName);
            businessWebsite = view.findViewById(R.id.textViewBusinessWebsite);
            businessPhoneNum = view.findViewById(R.id.textViewBusinessPhoneNum);

            view.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    // TODO Auto-generated method stub
                    Utility.showDialog((Activity)v.getContext(), "Business Details",
                            businessList.get(getAdapterPosition()).toString());
                    return true;
                }
            });
            //Once we click this row we will want to show more details about this camper.
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Log.d("MAXMAC", "Clicked a business row. "+getAdapterPosition());
                    Log.d("MAXMAC", "Business details: "+businessList.get(getAdapterPosition()).toString());

                    //used this resource for the following syntax:  http://code.i-harness.com/en/q/1ba462f
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,
                            BusinessDetailsFragment.newInstance(businessList.get(getAdapterPosition()))).addToBackStack(null).commit();
                }
            });
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }

}